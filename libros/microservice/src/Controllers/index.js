const { Model } = require('./../Models')

const Create = async ({ title }) => {
  try {
    const instance = await Model.create(
      { title }, 
      { logging: false }
    )

    return { statusCode: 200, data: instance.toJSON() }
  } catch (error) {
    console.log({ step: 'controller Create', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

const Delete = async ({ where = { id } }) => {
  try {
    await Model.destroy({ where, logging: false })

    return { statusCode: 200, data: 'ok' }
  } catch (error) {
    console.log({ step: 'controller Delete', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

const Update = async ({ title, category, sections, id }) => {
  try {
    const instance = await Model.update(
      { title, category, sections }, 
      { where: { id }, logging: false, returning: true }
    )

    return { statusCode: 200, data: instance[1][0].toJSON() }
  } catch (error) {
    console.log({ step: 'controller Update', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

const FindOne = async ({ where = { title } }) => {
  try {
    const instance = await Model.findOne({ where, logging: false })
    if(instance) return { statusCode: 200, data: instance.toJSON() }
    return { statusCode: 400, message: 'Campaign does not exist' }
  } catch (error) {
    console.log({ step: 'controller FindOne', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

const View = async ({ where = {} }) => {
  try {
    const instances = await Model.findAll({ where, logging: false })

    return { statusCode: 200, data: instances }
  } catch (error) {
    console.log({ step: 'controller View', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

module.exports = { Create, Update, Delete, FindOne, View }