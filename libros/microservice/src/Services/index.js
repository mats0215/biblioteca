const Controllers = require('./../Controllers')
const { InternalError } = require('./../settings')

const Create = async ({ title }) => {
  try {
    const { statusCode, data, message } = await Controllers.Create({ title })

		return { statusCode, data, message }
  } catch (error) {
    console.log({ step: 'services Create', error: error.toString() })
		return { statusCode: 500, message: error.toString() }
  }
}

const Delete = async ({ id }) => {
  try {
    const findOne = await Controllers.FindOne({ where: { id } })

    if(findOne.statusCode !== 200) {
      const response = {
        400: { statusCode: 400, message: 'Campaign does not exist Delete' },
        500: { statusCode: 500, message: InternalError }
      }

      return response[findOne.statusCode]
    }

    const del = await Controllers.Delete({ where: { id } })

    if(del.statusCode === 200) return { statusCode: 200, data: findOne.data }

		return { statusCode: 400, message: InternalError }
  } catch (error) {
    console.log({ step: 'services Delete', error: error.toString() })
		return { statusCode: 500, message: error.toString() }
  }
}

const Update = async ({ title, category, sections, id }) => {
  try {
    const findOne = await Controllers.FindOne({ where: { id } })

    if(findOne.statusCode !== 200) {
      const response = {
        400: { statusCode: 400, message: 'Campaign does not exist Update' },
        500: { statusCode: 500, message: InternalError }
      }

      return response[findOne.statusCode]
    }

    const { statusCode, data, message } = await Controllers.Update({ title, category, sections, id })

		return { statusCode, data, message }
  } catch (error) {
    console.log({ step: 'services Update', error: error.toString() })
		return { statusCode: 500, message: error.toString() }
  }
}

const FindOne = async ({ title }) => {
  try {
    const { statusCode, data, message } = await Controllers.FindOne({ where: { title } })

		return { statusCode, data, message }
  } catch (error) {
    console.log({ step: 'services FindOne', error: error.toString() })
		return { statusCode: 500, message: error.toString() }
  }
}

const View = async ({}) => {
  try {
    const { statusCode, data, message } = await Controllers.View({})

		return { statusCode, data, message }
  } catch (error) {
    console.log({ step: 'services View', error: error.toString() })
		return { statusCode: 500, message: error.toString() }
  }
}

module.exports = { Create, Delete, Update, FindOne, View }