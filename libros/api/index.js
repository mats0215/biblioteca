const { name } = require('./package.json')
const bull = require('bull')

const options = { redis: { host: '192.168.100.25', port: 6379 } }

const queueCreate = bull(`${name}:create`, options)
const queueDelete = bull(`${name}:delete`, options)
const queueUpdate = bull(`${name}:update`, options)
const queueFindOne = bull(`${name}:findOne`, options)
const queueView = bull(`${name}:view`, options)

const Create = async ({ title }) => {
  try {
    const job = await queueCreate.add({ title })
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const Delete = async ({ id }) => {
  try {
    const job = await queueDelete.add({ id })
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const Update = async ({ title, category, sections, id }) => {
  try {
    const job = await queueUpdate.add({ title, category, sections, id })
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const FindOne = async ({ title }) => {
  try {
    const job = await queueFindOne.add({ title })
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const View = async ({}) => {
  try {
    const job = await queueView.add({})
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const main = async () => {
  // await Create({
  //   name: 'Mar', 
  //   descripcion: 'Campaign', 
  //   url: 'www.google.com', 
  //   video: 'https://www.youtube.com/watch?v=zayX1YXiP6Y&list=RDzayX1YXiP6Y&start_radio=1', 
  //   imagen: 'image.jpg'
  // })

  // await Update({
  //   name: 'Luis MOD', 
  //   descripcion: 'Campaign', 
  //   url: 'www.google.com', 
  //   video: 'https://www.youtube.com/watch?v=zayX1YXiP6Y&list=RDzayX1YXiP6Y&start_radio=1', 
  //   imagen: 'image.jpg',
  //   id: 1
  // })

  // await FindOne({ id: 1 })

  // await Delete({ id: 3 })

  // await View({})
}

module.exports = { Create, Delete, Update, FindOne, View }