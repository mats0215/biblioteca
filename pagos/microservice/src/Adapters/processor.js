const Services = require('../Services')
const { InternalError } = require('./../settings')
const { queueCreate, queueDelete, queueView, queueFindOne } = require('./index')

const Create = async (job, done) => {
  try {
    const { socio, amount } = job.data
    const { statusCode, data, message } = await Services.Create({ socio, amount })

    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ step: 'adapter queueCreate', error: error.toString() })
    done(null, { statusCode: 500, message: InternalError })
  }
}

const Delete = async (job, done) => {
  try {
    const { id } = job.data
    const { statusCode, data, message } = await Services.Delete({id})

    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ step: 'adapter queueDelete', error: error.toString() })
    done(null, { statusCode: 500, message: InternalError })
  }
}

const FindOne = async (job, done) => {
  try {
    const { id } = job.data
    const { statusCode, data, message } = await Services.FindOne({ id })

    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ step: 'queueFindOneAdaptador', error: error.toString() })
    done(null, { statusCode: 500, message: InternalError })
  }
}

const View = async (job, done) => {
  try {
    console.log(job.id)

    const {} = job.data
    const { statusCode, data, message } = await Services.View({})

    // done(null, { statusCode, data, message })
    done( null, { statusCode, data: data.map(  v => ({...v.toJSON(), ...{ worker: job.id } })  ), message })
  } catch (error) {
    console.log({ step: 'queueViewAdaptador', error: error.toString() })
    done(null, { statusCode: 500, message: InternalError })
  }
}

const run = async () => {
  try {
    console.log('initialized worker')

    queueCreate.process(Create)
    queueDelete.process(Delete)
    queueFindOne.process(FindOne)
    queueView.process(View)
  } catch (error) {
    console.log(error)
  }
}

module.exports = { Create, Delete, View, run }