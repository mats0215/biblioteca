## API de Pagos en la biblioteca

Esta api permite la interaccion con los pagos registrados en la biblioteca
Las funciones permitidas son las siguientes:

```js
socket.on('req:pagos:create', async ({ socio, amount }) => {
  try {
    console.log('req:pagos:create', ({ socio, amount }))

    const { statusCode, data, message } = await apiPagos.Create({ socio, amount })
    return io.to(socket.id).emit('res:pagos:create', { statusCode, data, message })
  } catch (error) {
    console.log(error)
  }
})

socket.on('req:pagos:delete', async ({ id }) => {
  try {
    console.log('req:pagos:delete', ({ id }))

    const { statusCode, data, message } = await apiPagos.Delete({ id })
    return io.to(socket.id).emit('res:pagos:delete', { statusCode, data, message })
  } catch (error) {
    console.log(error)
  }
})

socket.on('req:pagos:findOne', async ({ id }) => {
  try {
    console.log('req:pagos:findOne', ({ id }))

    const { statusCode, data, message } = await apiPagos.FindOne({ id })
    return io.to(socket.id).emit('res:pagos:findOne', { statusCode, data, message })
  } catch (error) {
    console.log(error)
  }
})

socket.on('req:pagos:view', async ({}) => {
  try {
    console.log('req:pagos:view', ({}))

    const { statusCode, data, message } = await apiPagos.View({})
    return io.to(socket.id).emit('res:pagos:view', { statusCode, data, message })
  } catch (error) {
    console.log(error)
  }
})