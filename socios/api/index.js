const { name } = require('./package.json')
const bull = require('bull')

const options = { redis: { host: '127.0.0.1', port: 6379 } }

const queueCreate = bull(`${ name }:create`, options)
const queueDelete = bull(`${ name }:delete`, options)
const queueUpdate = bull(`${ name }:update`, options)
const queueFindOne = bull(`${ name }:findOne`, options)
const queueView = bull(`${ name }:view`, options)
const queueEnable = bull(`${ name }:enable`, options)
const queueDisable = bull(`${ name }:disable`, options)

const Create = async ({ name, phone }) => {
  try {
    const job = await queueCreate.add({ name, phone })
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const Delete = async ({ id }) => {
  try {
    const job = await queueDelete.add({ id })
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const Update = async ({ name, age, email, phone, id }) => {
  try {
    const job = await queueUpdate.add({ name, age, email, phone, id })
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const FindOne = async ({ id }) => {
  try {
    const job = await queueFindOne.add({ id })
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const View = async ({ enable }) => {
  try {
    const job = await queueView.add({ enable })
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const Enable = async ({ id }) => {
  try {
    const job = await queueEnable.add({ id })
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const Disable = async ({ id }) => {
  try {
    const job = await queueDisable.add({ id })
    const { statusCode, data, message } = await job.finished()
    
    return { statusCode, data, message }
  } catch (error) {
    console.log(error)
  }
}

const main = async () => {
  // await Create({
  //   name: 'Mar', 
  //   descripcion: 'Campaign', 
  //   url: 'www.google.com', 
  //   video: 'https://www.youtube.com/watch?v=zayX1YXiP6Y&list=RDzayX1YXiP6Y&start_radio=1', 
  //   imagen: 'image.jpg'
  // })

  // await Update({
  //   name: 'Luis MOD', 
  //   descripcion: 'Campaign', 
  //   url: 'www.google.com', 
  //   video: 'https://www.youtube.com/watch?v=zayX1YXiP6Y&list=RDzayX1YXiP6Y&start_radio=1', 
  //   imagen: 'image.jpg',
  //   id: 1
  // })

  // await FindOne({ id: 1 })

  // await Delete({ id: 3 })

  // await View({})
}

module.exports = { Create, Delete, Update, FindOne, View, Enable, Disable }