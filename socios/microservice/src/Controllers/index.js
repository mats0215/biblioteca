const { Model } = require('./../Models')

const Create = async ({ name, phone }) => {
  try {
    const instance = await Model.create(
      { name, phone }, 
      { logging: false }
    )

    return { statusCode: 200, data: instance.toJSON() }
  } catch (error) {
    console.log({ step: 'controller Create', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

const Delete = async ({ where = { id } }) => {
  try {
    await Model.destroy({ where, logging: false })

    return { statusCode: 200, data: 'ok' }
  } catch (error) {
    console.log({ step: 'controller Delete', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

const Update = async ({ name, age, email, phone, id }) => {
  try {
    const instance = await Model.update(
      { name, age, email, phone }, 
      { where: { id }, logging: false, returning: true }
    )

    return { statusCode: 200, data: instance[1][0].toJSON() }
  } catch (error) {
    console.log({ step: 'controller Update', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

const FindOne = async ({ where = { id } }) => {
  try {
    const instance = await Model.findOne({ where, logging: false })
    if(instance) return { statusCode: 200, data: instance.toJSON() }
    return { statusCode: 400, message: 'Campaign does not exist' }
  } catch (error) {
    console.log({ step: 'controller FindOne', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

const View = async ({ where = { enable } }) => {
  try {
    const instances = await Model.findAll({ where, logging: false })

    return { statusCode: 200, data: instances }
  } catch (error) {
    console.log({ step: 'controller View', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

const Enable = async ({ where = { id } }) => {
  try {
    const instances = await Model.findAll({ where, logging: false })

    return { statusCode: 200, data: instances }
  } catch (error) {
    console.log({ step: 'controller Enable', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

const Disable = async ({ where = { id } }) => {
  try {
    const instances = await Model.findAll({ where, logging: false })

    return { statusCode: 200, data: instances }
  } catch (error) {
    console.log({ step: 'controller Disable', error: error.toString() })
		return { statusCode: 400, message: error.toString() }
  }
}

module.exports = { Create, Update, Delete, FindOne, View, Enable, Disable }