const Services = require('../Services')
const { InternalError } = require('./../settings')
const { queueCreate, queueDelete, queueUpdate, queueView, queueFindOne, queueEnable, queueDisable } = require('./index')

const Create = async (job, done) => {
  try {
    const { name, phone } = job.data
    const { statusCode, data, message } = await Services.Create({ name, phone })

    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ step: 'adapter queueCreate', error: error.toString() })
    done(null, { statusCode: 500, message: InternalError })
  }
}

const Delete = async (job, done) => {
  try {
    const { id } = job.data
    const { statusCode, data, message } = await Services.Delete({id})

    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ step: 'adapter queueDelete', error: error.toString() })
    done(null, { statusCode: 500, message: InternalError })
  }
}

const Update = async (job, done) => {
  try {
    const { name, age, email, phone, id } = job.data
    const { statusCode, data, message } = await Services.Update({ name, phone, age, email, id })

    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ step: 'adapter queueUpdate', error: error.toString() })
    done(null, { statusCode: 500, message: InternalError })
  }
}

const FindOne = async (job, done) => {
  try {
    const { id } = job.data
    const { statusCode, data, message } = await Services.FindOne({ id })

    done(null, { statusCode, data, message })
  } catch (error) {
    console.log({ step: 'queueFindOneAdaptador', error: error.toString() })
    done(null, { statusCode: 500, message: InternalError })
  }
}

const View = async (job, done) => {
  try {
    const { enable } = job.data
    const { statusCode, data, message } = await Services.View({ enable })

    done( null, { statusCode, data: data, message })
  } catch (error) {
    console.log({ step: 'queueViewAdaptador', error: error.toString() })
    done(null, { statusCode: 500, message: InternalError })
  }
}

const Enable = async (job, done) => {
  try {
    const { id } = job.data
    const { statusCode, data, message } = await Services.Enable({ id })

    done( null, { statusCode, data: data, message })
  } catch (error) {
    console.log({ step: 'queueEnableAdaptador', error: error.toString() })
    done(null, { statusCode: 500, message: InternalError })
  }
}

const Disable = async (job, done) => {
  try {
    const { id } = job.data
    const { statusCode, data, message } = await Services.Disable({ id })

    done( null, { statusCode, data: data, message })
  } catch (error) {
    console.log({ step: 'queueDisableAdaptador', error: error.toString() })
    done(null, { statusCode: 500, message: InternalError })
  }
}

const run = async () => {
  try {
    console.log('initialized worker')

    queueCreate.process(Create)
    queueDelete.process(Delete)
    queueUpdate.process(Update)
    queueFindOne.process(FindOne)
    queueView.process(View)
    queueEnable.process(View)
    queueDisable.process(View)
  } catch (error) {
    console.log(error)
  }
}

module.exports = { Create, Delete, Update, View, Enable, Disable, run }