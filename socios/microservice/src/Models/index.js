const { sequelize } = require('./../settings')
const { DataTypes } = require('sequelize')

const Model = sequelize.define('socio', {
	name: { type: DataTypes.STRING },
	age: { type: DataTypes.BIGINT },
	email: { type: DataTypes.STRING },
	phone: { type: DataTypes.STRING },
	enable: { type: DataTypes.BOOLEAN }
}, { freezeTableName: true })

const SyncDB = async () => {
	try {
		console.log('initializing database')

		await Model.sync({ logging: false })
		console.log('initialized database')

		return { statusCode: 200, data: 'ok' }
	} catch (error) {
		console.log(error)
		return { statusCode: 500, message: error.toString() }
	}
}

module.exports = { SyncDB, Model }